package com.pym.fishingboatapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FishingBoatApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FishingBoatApiApplication.class, args);
	}

}
