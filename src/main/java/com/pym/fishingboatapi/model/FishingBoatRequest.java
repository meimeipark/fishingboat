package com.pym.fishingboatapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FishingBoatRequest {
    private String boatName;
    private String admin;
    private String phoneNumber;
    private String dock;
}
